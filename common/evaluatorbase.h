#ifndef EVALUATORBASE_H
#define EVALUATORBASE_H
#include "evfunc.h"
#include "evvar.h"
#include "stdio.h"
#include "math.h"
#include <stdlib.h>


class evaluatorBase
{
public:
    evaluatorBase(char *fonk, char *degs);
    ~evaluatorBase();


        evFunc*fx[100];
        evVar vars[100];




        struct eleman{
        char isim[200];
        char g1;
        char g2;
        char g3;
        };

        eleman elemanlar[1000];
        int elemansay;





        void koda_ekle(char *isim, char g1, char g2,char g3, int eklemeyeri,int uzerineyaz);
        void koddan_sil(int no);
        char*aradan_al(char*k,int bas, int uz);

        int sayimi(char*x);
        int sayisalmi(char*x);
        int sgn(float x);
        int fonksiyonsay;

        char errorStr[100];


    public:


        int make(const char*isl, evFunc *fnc);//new version, supports multi exp!
        int make2(const char*program,evFunc*fnc);
        int makeLine(char*expression, evFunc *fnc);

        int isle(evFunc*islem);


        void fonksiyonlari_yukle();
        int fnobul(char*isim);
        void load_function(evFunc*fnc,int fNum=0);


        int degisken_ekle(const char*isim, double deger, int sabit,int yer);
        int deg_no_bul(const char*isim);

        void degisken_sil(int deg_no);
        void degisken_sil(const char*isim);
        double getVariableValue(int Vno){return vars[Vno].getValue();}

        void degiskenleri_yukle();
        evVar variable(int varNo);
        void degisken_ekle(evVar*dgs,int adres);
        void saveVarsToFile();
        void setVarValue(int vNo,float Value){vars[vNo].setValue(Value);}
        char*getErrorStr(){return errorStr;}

        char function_file[100];
        char variable_file[100];

        int num_of_functions();

        evFunc*get_function(int fno);

        //KEDÝ2

        float REGS[200];
        int STACK[200][2];
        int PC,SP,fno;


        private:



        int evaluatorBase_Derle(char*derlenecek);
        int harfbul(char*k,char harf,int basl);
        int sonrakiKarakter(char*k,int basl);//bosluklar haric..
        void boslukSil(char*k);
        int sonrakiParca(char*k,int konum,int*tur);
        bool bosluk(char*k, int bas, int son);

        char derlenecek[1000];
        int kodboyu;





        struct kodParcasi{

            char*kod;
            int derece;
            int gorev; //-1:yorum,//0:düz kod, 1:koþul,2:for dongü baþlangýcý,3:for döngükoþulu, 4:for döngü iþlemi:
            //5:while döngükoþulu

        };



        kodParcasi KP[1000];

        char*emirler[500];
        char*satirlar[1000];

        int els,sats;
        int adresler[1000][2];
        char turler[1000];


        FILE*fdebug;
};


#endif // EVALUATORBASE_H
