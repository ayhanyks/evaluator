#ifndef EVFUNC_H
#define EVFUNC_H

#include "string.h"

class evFunc
{
public:
        char*Name;
        int InputNum;
        int CodeSize;
        int MemoryInputNum;
        int*Code;
        double*Memory;
        int AnswerAddress;
        int MaxCodeSize;
        int MaxMemorySize;
        char*ACIKLAMA;



        evFunc();
        virtual ~evFunc();
        evFunc(char*name,int maxCodesize,int maxMemorySize);

        void setInputNum(int inputNum){InputNum=inputNum;}
        int  getInputNum(){return InputNum;}

        void setAnswerAdress(int answerAddress){AnswerAddress=answerAddress;}
        int  getAnswerAddress(){return AnswerAddress;}

        void setMemoryInputNum(int memoryInputNum){MemoryInputNum=memoryInputNum;}
        int  getMemoryInputNum(){return MemoryInputNum;}

        double getMemory(int address){return Memory[address];}
        void   setMemory(int address, double value){Memory[address]=value;}

        void setCode(int address,int value){Code[address]=value;}
        int  getCode(int address){return Code[address];}

        void incMIN(int inc=1){MemoryInputNum+=inc;}
        void incCSize(int inc=1){CodeSize+=inc;}
        void setCodeSize(int codeSize){CodeSize=codeSize;}
        int  getCodeSize(){return CodeSize;}

        void setName(char*name){strcpy(Name,name);}
        char*getName(){return Name;}

        void aciklamaVer(char* aciklama){strcpy(ACIKLAMA,aciklama);}
        char*aciklamaAl(){return ACIKLAMA;}

        void addCode(int code){Code[CodeSize]=code;CodeSize++;}
        void addCode(int c1,int c2){Code[CodeSize]=c1;Code[CodeSize+1]=c2;CodeSize+=2;}
        void addCode(int c1,int c2,int c3){Code[CodeSize]=c1;Code[CodeSize+1]=c2;Code[CodeSize+2]=c3;CodeSize+=3;}
        void addCode(int c1,int c2,int c3,int c4){Code[CodeSize]=c1;Code[CodeSize+1]=c2;
                Code[CodeSize+2]=c3;Code[CodeSize+3]=c4;CodeSize+=4;}


        void clear();
        void boyutla(int maxCodeSize=256,int maxMemorySize=256);
};

#endif // EVFUNC_H
