#include "evfunc.h"




evFunc::evFunc()
{
    Name=new char[1000];
    ACIKLAMA=new char[1000];

    ACIKLAMA[0]=0;
    Name[0]=0;

    Code=new int[1000];
    MaxCodeSize=1000;
    MaxMemorySize=1000;

    Memory=new double[1000];
    MemoryInputNum=0;
    CodeSize=0;
    InputNum=0;

}


evFunc::evFunc(char*name,int maxCodesize,int maxMemorySize)
{
    Name=new char[strlen(name)];
    strcpy(Name,name);
    Code=new int[maxCodesize];
    Memory=new double[maxMemorySize];
    MemoryInputNum=0;
    CodeSize=0;
    InputNum=0;
    MaxCodeSize=maxCodesize;
    MaxMemorySize=maxMemorySize;

}


evFunc::~evFunc()
{


}

void evFunc::clear(){

    if(MaxCodeSize>0)
        delete Code;

    if(MaxMemorySize>0)
        delete Memory;


    if(strlen(Name)>0)
        delete Name;

    if(strlen(ACIKLAMA)>0)
        delete ACIKLAMA;

    CodeSize=0;
    MemoryInputNum=0;
    AnswerAddress=0;
    MaxCodeSize=0;
    MaxMemorySize=0;

}

void evFunc::boyutla(int maxCodeSize,int maxMemorySize){

    clear();
    Code=new int[maxCodeSize];
    Memory=new double[maxMemorySize];
    MemoryInputNum=0;
    CodeSize=0;
    InputNum=0;
    MaxCodeSize=maxCodeSize;
    MaxMemorySize=maxMemorySize;


}
