#ifndef EVVAR_H
#define EVVAR_H
#include "string.h"


class evVar
{
public:

    double Value;
    char   Name[20];
    int    Fixed;
    int		Enabled;


    evVar();

        int  getFixed(){return Fixed;}
    void setFixed(int fixed){Fixed=fixed;}

    int getEnabled(){return Enabled;}
    void setEnabled(int enabled){Enabled=enabled;}

    double getValue(){return Value;}
    void   setValue(double value){Value=value;}

    char*getName(){return Name;}
    void setName(const char*name){strcpy(Name,name);}
};

#endif // EVVAR_H
