#include "evaluatorbase.h"

#define KDEBUG 1

evaluatorBase::evaluatorBase(char*fonk, char*degs)
{
    int i;
    fonksiyonsay=0;
    elemansay=0;

    for(i=fonksiyonsay;i<100;i++)
        fx[i]=new evFunc;


   // strcpy(function_file,"/mnt/win_c/fonksiyonlar.txt");
  //  strcpy(variable_file,"/mnt/win_c/degiskenler.txt");

    strcpy(function_file,fonk);
    strcpy(variable_file,degs);

#if KDEBUG==1
    fdebug=fopen("Kdebug.txt","w");
    fprintf(fdebug,"evBase debug file\n");
#endif


    fonksiyonlari_yukle();
    degiskenleri_yukle();

    PC=0;
    SP=0;
    fno=0;


}

evaluatorBase::~evaluatorBase()
{
#if KDEBUG==1
    fclose(fdebug);
#endif

int i;
}


int evaluatorBase::make(const char*program,evFunc*fnc){


    int size=strlen(program);

    if(!size)
        return 0;


    fnc->boyutla(size*10,size*10);



    char*PRG;
    int i,lineStart=0,lineEnd=0,lineNumber=0;



    int*linePlaces;
    PRG=new char[size];

    linePlaces=new int[size/2+2];


    linePlaces[0]=0;
    char curLine[100];
    char c;

    if(size<0)
        return -1;

    size=0;

    for(i=0;program[i];i++){
        c=program[i];
        if(c!=10 && c!=32 && c!=13){
            PRG[size]=c;
            size++;
        }

    }


    for(i=0;i<size;i++)
    if(PRG[i]==';'){
        lineNumber++;
        linePlaces[lineNumber]=i+1;
        }




    if(lineNumber==0){
        linePlaces[1]=size+1;
        lineNumber=1;
    }



    for(i=0;i<lineNumber;i++){
    strcpy(curLine,aradan_al(PRG,linePlaces[i],linePlaces[i+1]-linePlaces[i]-1));
    if(makeLine(curLine,fnc)<0)
        return -1;


    }

    fnc->addCode(99,fnc->getMemoryInputNum()-1);//program� bitirir



    delete PRG;
    delete linePlaces;


    return 1;


}



#define debugmake2 1
int evaluatorBase::make2(const char*program,evFunc*fnc){



    int size=strlen(program),i;
        kodboyu=size;
    if(!size)
        return -1;


    fnc->boyutla(size*10,size*10);

#if debugmake2==1 && KDEBUG==1
    fprintf(fdebug,"MAKE2> ayarlanan boyut:%d\n",size*10);
#endif

    int baslamalar[1000];
    int  bekleyenAdresler[1000]={0};


    strcpy(derlenecek,program);


    int h=evaluatorBase_Derle(derlenecek);




    if(h<0){

        #if debugmake2==1 && KDEBUG==1
                fprintf(fdebug,"MAKE2> chapter1 failed!\n");
        #endif

        return -1;
    }
    else{
        #if debugmake2==1 && KDEBUG==1
                fprintf(fdebug,"MAKE2> chapter1 achieved!\n");
        #endif

    }
    for(i=0;i<els;i++){

        baslamalar[i]=fnc->CodeSize;

        if(turler[i]<30){

            if(makeLine(emirler[i],fnc)<0){
                    #if debugmake2==1 && KDEBUG==1
                        fprintf(fdebug,"MAKE2> %s derlenemedi :(\n",emirler[i]);
                    #endif
                    return -1;

            }
            else
            {

                    #if debugmake2==1 && KDEBUG==1
                        fprintf(fdebug,"MAKE2> %s derlendi :) baslama adresi=%d, memin=%d\n",emirler[i],baslamalar[i],fnc->MemoryInputNum);
                    #endif
            }
        }



        if(turler[i]==0){
            if(adresler[i][0]!=i+1){
                    #if debugmake2==1 && KDEBUG==1
                        fprintf(fdebug,"MAKE2> %s  icin atlama kodu gomuluyor\n",emirler[i]);
                    #endif

                    fnc->addCode(18,0);
                    bekleyenAdresler[i]=fnc->getCodeSize();

            }

        }

        if(turler[i]==1){ //if kosulu cumlesi..
                //kosullu olarak atlama oldugu icin, buraya bir atlama kodu g�mecez..
                //atlama adresi yalniz hala belli degil..
                //ilk once on cevap kontrol edilecek,
                //sifir ise 1.adrese,
                //degil ise 2.adrese atla..
                    #if debugmake2==1 && KDEBUG==1
                        fprintf(fdebug,"MAKE2> if kosulu icin ekstra kod gomuluyor\n");
                    #endif

                    fnc->addCode(20,fnc->getMemoryInputNum()-1,0);//su anda adres belli degil ki?
                    bekleyenAdresler[i]=fnc->getCodeSize();
        }

        if(turler[i]==2){ //1.adresi atlamali olan if..
                    #if debugmake2==1 && KDEBUG==1
                        fprintf(fdebug,"MAKE2> if kosulu icin ekstra kod gomuluyor\n");
                    #endif

                    fnc->addCode(19,fnc->getMemoryInputNum()-1,0);//su anda adres belli degil ki?
                    bekleyenAdresler[i]=fnc->getCodeSize();

        }
        if(turler[i]==3){	//while kosulu c�mlesi, ifin aynisi..
                    #if debugmake2==1 && KDEBUG==1
                        fprintf(fdebug,"MAKE2> if kosulu icin ekstra kod gomuluyor\n");
                    #endif
                    fnc->addCode(20,fnc->getMemoryInputNum()-1,0);//su anda adres belli degil ki?
                    bekleyenAdresler[i]=fnc->getCodeSize();
        }
        if(turler[i]==32){//ozel kosulsuz atlama komutu

                    #if debugmake2==1 && KDEBUG==1
                        fprintf(fdebug,"MAKE2> ozel komut icin atlama kod gomuluyor\n");
                    #endif

                    fnc->addCode(18,0);
                    bekleyenAdresler[i]=fnc->getCodeSize();




        }

    }

    //sonlandirma:

    baslamalar[i]=fnc->CodeSize;
    fnc->addCode(99,fnc->getMemoryInputNum()-1);//program� bitirir


    //bekleyen adresleri doldurma..
int j;


    for(i=0;i<els;i++){

            if(bekleyenAdresler[i]>0){

                if(turler[i]==0)
                    j=adresler[i][0];
                if(turler[i]==1)
                    j=adresler[i][1];
                if(turler[i]==2)
                    j=adresler[i][0];
                if(turler[i]==3)
                    j=adresler[i][1];

                if(turler[i]==32)
                    j=adresler[i][0];

                    #if debugmake2==1 && KDEBUG==1
                        fprintf(fdebug,"MAKE2> %s nin sonraki adresi veriliyor, %d...%d  \n",emirler[i],j,baslamalar[j]);
                    #endif

            fnc->Code[bekleyenAdresler[i]-1]=baslamalar[j];

        }



    }




        #if debugmake2==1 && KDEBUG==1
                        for(i=0;i<fnc->CodeSize;i++)
                            fprintf(fdebug,"%d\t",fnc->Code[i]);
        #endif






return 1;

}



void evaluatorBase::koddan_sil(int no){

    int i,j;
    if (elemansay==0)
    {
//	fprintf(fdebug,"elemansay=0\n");
    return;

    }


    for(i=no;i<elemansay-1;i++){

    for(j=0;j<sizeof(elemanlar[i].isim);j++)
        elemanlar[i].isim[j]=elemanlar[i+1].isim[j];

    elemanlar[i].g1=elemanlar[i+1].g1;
    elemanlar[i].g2=elemanlar[i+1].g2;
    elemanlar[i].g3=elemanlar[i+1].g3;

    }

    elemansay--;

}


void evaluatorBase::koda_ekle(char*isim,char g1,char g2,char g3, int eklemeyeri,int uzerineyaz)
{

    int i,j;

    i=eklemeyeri;
    if(i<0)
    i=elemansay;

    if(uzerineyaz){
    strcpy(elemanlar[i].isim,isim);

elemanlar[i].g1=g1;
elemanlar[i].g2=g2;
elemanlar[i].g3=g3;
    }
    else
    {
    for(j=elemansay;j>i;j--){
        strcpy(elemanlar[j].isim,elemanlar[j-1].isim);
        elemanlar[j].g1=elemanlar[j-1].g1;
        elemanlar[j].g2=elemanlar[j-1].g2;
        elemanlar[j].g3=elemanlar[j-1].g3;
    }



    strcpy(elemanlar[i].isim,isim);
    elemanlar[i].g1=g1;
    elemanlar[i].g2=g2;
    elemanlar[i].g3=g3;

    elemansay++;
    }


}


int evaluatorBase::fnobul(char*isim){

    int i;
    for(i=1;i<=fonksiyonsay;i++)
    if(!strcmp(isim,fx[i]->getName()))
        return i;

 return -1;

}




void evaluatorBase::fonksiyonlari_yukle(){

    FILE*dosya;
    char a[512],b[512],ctemp[512];
    int x,itemp;
    dosya=fopen(function_file,"r");
    int i,j,k;
    char tanimlar[100]={0};
    int  ts=100;


    while(!feof(dosya)){
    a[0]=0;//to clear a;

    fscanf(dosya,"%s",a);

    if(strcmp("FONKSIYON",a)==0){
        fonksiyonsay++;
        fscanf(dosya,"%s",ctemp);fx[fonksiyonsay]->setName(ctemp);
    }

    if(strcmp("ACIKLAMA",a)==0){
        fscanf(dosya,"%s",ctemp);fx[fonksiyonsay]->aciklamaVer(ctemp);
    }

    if(strcmp("GIRISSAY",a)==0)
       fscanf(dosya,"%d",&itemp);fx[fonksiyonsay]->setInputNum(itemp);


    if(strcmp("TANIMLAMA",a)==0){

        while(1){
            fscanf(dosya,"%s",&b);
            if(strcmp(b,"son")==0)
                break;

            if(b[0]=='#')
                continue;

            j=b[0];
            fscanf(dosya,"%s",&b);
            k=atoi(b);
            tanimlar[k]=j;

        }


    }

    if (strcmp("KOD",a)==0){

        i=0;
        char KayitliYerler[100]={0};
        int Kys=0,Gys=0;
        char GidilecekYerler[100]={0};



            while(1){
            fscanf(dosya,"%s",&b);
            if(strcmp(b,"son")==0)
                break;
            else if(b[strlen(b)-1]=='$'){
                b[strlen(b)-1]=0;
                x=fnobul(b);
            }
            else if(b[strlen(b)-1]==':'){
                KayitliYerler[Kys]=b[0];
                KayitliYerler[Kys+1]=i;
                Kys+=2;
                continue;
            }
            else if(b[0]=='@'){
                GidilecekYerler[Gys]=b[1];
                GidilecekYerler[Gys+1]=i;
                Gys+=2;
            }
            else if(b[0]=='#')
                continue;

            else if(b[0]=='.'){
                for(j=0;j<ts;j++)
                    if(tanimlar[j]==b[1])
                        x=j;
            }


            else
                x=atoi(b);

            fx[fonksiyonsay]->setCode(i,x);

            i++;

        }
        char c;

        for(j=0;j<Gys;j+=2){
            c=GidilecekYerler[j];
            for(k=0;k<Kys;k+=2)
                if(c==KayitliYerler[k])
                    break;

            if(k<Kys)
                fx[fonksiyonsay]->setCode(GidilecekYerler[j+1],
                                    KayitliYerler[k+1]);
        }


        fx[fonksiyonsay]->setCodeSize(i);

    }

    if(strcmp("SABITLER",a)==0){
        i=0;
        while(1)
        {
        fscanf(dosya,"%s",&b);

        if(strcmp("son",b)==0)
        break;
        if (b[0]=='#')
        continue;

        else{

        fx[fonksiyonsay]->setMemory(i,atof(b));
        i++;

    }

    }
        fx[fonksiyonsay]->setMemoryInputNum(i);
    }



    }


    fclose(dosya);
/*
 fprintf(fdebug,"fonksiyonlar yuklendi\n");
 for(i=1;i<=fonksiyonsay;i++)
     fprintf(fdebug,"%s \t %d \n", fx[i].isim, fx[i].girissay);
 */


}


int evaluatorBase::degisken_ekle(const char*isim, double deger, int sabit,int yer){

      int i;
      i=deg_no_bul(isim);

      if (i>=0){
         vars[i].Value=deger;
         vars[i].setFixed(sabit);
      }


      for(i=0;i<100;i++)
      if(vars[i].getEnabled()==0)
          break;

      if(i<100){
      vars[i].setName(isim);
      vars[i].setFixed(sabit);
      vars[i].setEnabled(1);
      vars[i].setValue(deger);
      return i;

      }
      else
      return -1;


}

int evaluatorBase::deg_no_bul(const char*isim){

    int i;
    for(i=0;i<100;i++)
    if(strcmp(isim,vars[i].getName())==0)
        return i;


    return -1;


}

void evaluatorBase::degiskenleri_yukle(){

    FILE*dosya;
    char a[20];
    float c;
    int b;
    dosya=fopen(variable_file,"r");
    int i=0;

    while(!feof(dosya))
    {	fscanf(dosya,"%s",a);

    if(strcmp(a,"#end")==0)
        break;

    fscanf(dosya,"%d %f",&b,&c);


    vars[i].setName(a);
    vars[i].setValue(c);
    vars[i].setFixed(b);
    vars[i].setEnabled(1);

    i++;

    }

    while(i<100)
    vars[i++].setEnabled(0);


    fclose(dosya);

}

#define debugIsle	1
#define debugIslePause 0

int evaluatorBase::isle(evFunc*islem){


    fx[0]=islem;
    int*ki=fx[0]->Code;
    double*bi=fx[0]->Memory;
    int*kipc;

    PC=0;
    SP=0;
    fno=0;



while(1)
{
/*
#if debugIsle==1 && KDEBUG==1
    fprintf(fdebug,"konum=%d gorev=%d (par=%d %d %d)\n",PC,ki[PC],ki[PC+1],ki[PC+2],ki[PC+3]);
#endif

#if debugIslePause==1 && KDEBUG==1
    getchar();
#endif
*/
    kipc=&ki[PC];

switch(*kipc){//g�rev;

case 1:{//bellekten degisken yukle.


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d, bi[%d] <---- %s, deger= %f \n",*kipc, *(kipc+2),vars[*(kipc+1)].Name, vars[*(kipc+1)].Value);
        #endif



        bi[*(kipc+2)]=vars[*(kipc+1)].Value;
        PC+=3;
        break;
        }
case 2:{ // push

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d itme islemi icin %d;%d  SP=%d\n",*kipc, PC,fno,SP);
        #endif


        SP++;
        STACK[SP][0]=PC;
        STACK[SP][1]=fno;
        PC++;
        break;
        }
case 3:{ //pull


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d, cekme islemi icin %d;%d  SP=%d\n",*kipc, STACK[SP][0],STACK[SP][1],SP);
        #endif



        PC=STACK[SP][0];
        fno=STACK[SP][1];

        ki=fx[fno]->Code;
        bi=fx[fno]->Memory;

        SP--;
        PC+=2;
        break;
        }
case 4:{//k�t�k i�eri�ini fonksiyon belle�ine at
        //fx[fno]->setMemory(b,REGS[a]);


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d kutukten al bi[%d]<---- regs[%d] \n",*kipc, *(kipc+2), *(kipc+1) );
        #endif




        bi[*(kipc+2)]=REGS[*(kipc+1)];
        PC+=3;
        break;
        }
case 5:{//push fx mem to stack


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d fx mem to stack bi[%d]----> regs[%d] \n",*kipc, *(kipc+1), *(kipc+2) );
        #endif




        REGS[*(kipc+2)]=bi[*(kipc+1)];
        //REGS[b]=fx[fno]->getMemory(a);
        PC+=3;
        break;
        }
case 6:{//topla


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d topla, %f+%f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif



        bi[*(kipc+3)]=bi[*(kipc+1)]+bi[*(kipc+2)];
        PC+=4;
        break;
        }
case 7:{//��kar


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d cikar, %f-%f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif



        bi[*(kipc+3)]=bi[*(kipc+1)]-bi[*(kipc+2)];
        PC+=4;
        break;
        }
case 8:{//�arp


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d carp, %f*%f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif


        bi[*(kipc+3)]=bi[*(kipc+1)]*bi[*(kipc+2)];
        PC+=4;
        break;
        }

case 9:{//b�l

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d bol, %f:%f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif



        if(!bi[*(kipc+2)])
            return -1;

        bi[*(kipc+3)]=bi[*(kipc+1)]/bi[*(kipc+2)];
        PC+=4;
        break;

        }

case 10:{//kare k�k


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d karekok, %f \n",*kipc, bi[*(kipc+1)]);
        #endif



        if(bi[*(kipc+1)]<0)
            return -2;
        bi[*(kipc+2)]=sqrt(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 11:{//kare


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d kare, %f \n",*kipc, bi[*(kipc+1)]);
        #endif



        bi[*(kipc+2)]=pow(bi[*(kipc+1)],2);
        PC+=3;
        break;
        }

case 12:{//k�p k�k


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d kupkok, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=pow(bi[*(kipc+1)],0.3333);
        PC+=3;
        break;
        }


case 13:{//k�p

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d kup, %f \n",*kipc, bi[*(kipc+1)]);
        #endif



        bi[*(kipc+2)]=pow(bi[*(kipc+1)],3);
        PC+=3;
        break;
        }

case 14:{//sin


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d sin, %f \n",*kipc, bi[*(kipc+1)]);
        #endif



        bi[*(kipc+2)]=sin(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 15:{//cos



        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d cos, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=cos(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 16:{//tan

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d tan, %f \n",*kipc, bi[*(kipc+1)]);
        #endif



        bi[*(kipc+2)]=tan(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 17:{//ln

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d ln, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=log(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 18:{//atla

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d ATLA--> %d \n",*kipc,*(kipc+1));
        #endif



        PC=*(kipc+1);
        break;
        }

case 19:{//giri� >0 ise atla


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d %f>0 sa atla --> %d \n",*kipc,bi[*(kipc+1)],*(kipc+2));
        #endif


        if(bi[*(kipc+1)]>0)
            PC=*(kipc+2);
        else
            PC+=3;

        break;
        }


case 20:{ //giri� <=0 ise atla


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d %f<=0 sa atla --> %d \n",*kipc,bi[*(kipc+1)],*(kipc+2));
        #endif


        if(bi[*(kipc+1)]<=0)
            PC=*(kipc+2);
        else
            PC+=3;

        break;
        }

case 21:{//k�t�kten k�t��e kopyala


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d regs[%d]<--- regs[%d]=%f  \n",*kipc,*(kipc+2),*(kipc+1), REGS[*(kipc+1)]);
        #endif


        REGS[*(kipc+2)]=REGS[*(kipc+1)];
        PC+=3;
        break;
        }

case 22:{//bellekten belle�e kopyala

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d bi[%d]<--- bi[%d]=%f  \n",*kipc,*(kipc+2),*(kipc+1), bi[*(kipc+1)]);
        #endif

        bi[*(kipc+2)]=bi[*(kipc+1)];
        PC+=3;
        break;

        }

case 23:{//fonksiyon �a��r

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d fonksiyon cagir ;  %s \n",*kipc, fx[*(kipc+1)]->Name);
        #endif



        SP++;
        STACK[SP][0]=PC;
        STACK[SP][1]=fno;
        fno=*(kipc+1);
        PC=0;
        ki=fx[fno]->Code;
        bi=fx[fno]->Memory;

        break;
        }
case 24:{//�zeri

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d �zeri, %f^%f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif



        bi[*(kipc+3)]=pow(bi[*(kipc+1)],bi[*(kipc+2)]);
        PC+=4;
        break;
        }

case 25:{// < i�lemi


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d <?, %f, %f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif


        if (bi[*(kipc+1)]<bi[*(kipc+2)])
            bi[*(kipc+3)]=1;
        else
            bi[*(kipc+3)]=0;
        PC+=4;
        break;
        }
case 26:{// > islemi


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d >?, %f,%f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif


        if (bi[*(kipc+1)]>bi[*(kipc+2)])
            bi[*(kipc+3)]=1;
        else
            bi[*(kipc+3)]=0;
        PC+=4;
        break;
        }
case 27:{//: islemi


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d :?, %f,%f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif


        if (bi[*(kipc+1)]==bi[*(kipc+2)])
            bi[*(kipc+3)]=1;
        else
            bi[*(kipc+3)]=0;
        PC+=4;
        break;
        }
case 28:{



        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d ||, %f, %f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif


        if (bi[*(kipc+1)] || bi[*(kipc+2)])
            bi[*(kipc+3)]=1;
        else
            bi[*(kipc+3)]=0;
        PC+=4;
        break;
        }
case 29:{


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d &&, %f,%f \n",*kipc, bi[*(kipc+1)], bi[*(kipc+2)] );
        #endif


        if (bi[*(kipc+1)] && bi[*(kipc+2)])
            bi[*(kipc+3)]=1;
        else
            bi[*(kipc+3)]=0;
        PC+=4;
        break;
        }

case 30:{


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d atan, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=atan(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 31:{


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d abs, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=fabs(bi[*(kipc+1)]);
        PC+=3;
        break;
        }


case 32:{


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d exp, %f \n",*kipc, bi[*(kipc+1)]);
        #endif

        bi[*(kipc+2)]=exp(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 33:{// rast gele say�


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d rnd \n",*kipc);
        #endif


        bi[*(kipc+1)]=(float)rand()/RAND_MAX;//2147483647;
        PC+=2;
        break;
        }
case 34:{

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d int, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=int(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 35:{


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d sgn, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=sgn(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 36:{ //-1 le �arp


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d (-) %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=-bi[*(kipc+1)];
        PC+=3;
        break;
        }
case 37:{ // 1/x yap


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d (1/) %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        if(!bi[*(kipc+1)])
            return -3;

        bi[*(kipc+2)]=1/(bi[*(kipc+1)]);
        PC+=3;
        break;

        }

case 38:{//e�itse atla

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d esitse atla, %f,%f ---->%d\n",*kipc, bi[*(kipc+1)],bi[*(kipc+2)],*(kipc+3));
        #endif


        if(bi[*(kipc+1)]==bi[*(kipc+2)])
            PC=*(kipc+3);
        else
            PC+=4;
        break;
        }
case 39:{ //mod

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d mod, %f,%f \n",*kipc, bi[*(kipc+1)],bi[*(kipc+2)]);
        #endif


        bi[*(kipc+3)]=((int)bi[*(kipc+1)] % (int)bi[*(kipc+2)] );
        PC+=4;
        break;
        }

case 40:{ //s�f�rla
        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d sifirla",*kipc);
        #endif


        bi[*(kipc+1)]=0;
        PC+=2;
        break;
        }
case 41:{//artt�r

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d artir, %f,%f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=bi[*(kipc+1)]+1;
        PC+=3;
        break;
        }
case 42:{//azalt

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d azalt, %f,%f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=bi[*(kipc+1)]-1;
        PC+=3;
        break;
        }
case 43:{//de�il

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d degili, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        if(!bi[*(kipc+1)])
            bi[*(kipc+2)]=1;
        else
            bi[*(kipc+2)]=0;
        PC+=3;
        break;
        }
case 44:{//bitir

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d hata ile bitir.. \n",*kipc);
        #endif


        return -1;
        break;
        }

case 45:{//ikiye b�l

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d ikiye bol, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=bi[*(kipc+1)]/2;
        PC+=3;
        break;
        }


case 46:{//de�i�kene kaydet

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d degiskene at, %f ---> %s \n",*kipc, bi[*(kipc+1)], vars[*(kipc+2)].Name);
        #endif


        vars[*(kipc+2)].Value=bi[*(kipc+1)];
        PC+=3;
        break;
    }


case 47:{//giri� <0 ise atla

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d sifirdan kucukse atla, %f ---->%d\n",*kipc, bi[*(kipc+1)], *(kipc+2));
        #endif


        if(bi[*(kipc+1)]<0)
            PC=*(kipc+2);
        else
            PC+=3;

        break;
        }
case 48:{//giri�in kesri

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d kesri, %f \n",*kipc, bi[*(kipc+1)]);
        #endif


        bi[*(kipc+2)]=bi[*(kipc+1)]-int(bi[*(kipc+1)]);
        PC+=3;
        break;
        }

case 49:{//belle�e kodda yazilan sayiyi ata


        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d bellege koddaki sayiyi at %d, %d \n",*kipc, *(kipc+2),*(kipc+1));
        #endif


        bi[*(kipc+2)]=*(kipc+1);
        PC+=3;
        break;
        }



case 99:{//bitir

        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d bitir...(99) \n",*kipc);
        #endif


        //*ans=bi[*(kipc+1)];
        vars[0].Value=bi[*(kipc+1)];//*ans;
    //	vars[fx[fno]->AnswerAddress].Value=bi[*(kipc+1)];
        return 1;

        }


default:{
        #if debugIsle==1 && KDEBUG==1
            fprintf(fdebug,"PC=%d hatali istek %d .. \n",*kipc, *kipc);
        #endif


        return -2;
        break;


        }


    }
 }

}


int evaluatorBase::sgn(float x)
{
    if(x>0)
    return 1;
    else if(x<0)
    return -1;
    else
    return 0;

}


char*evaluatorBase::aradan_al(char*k,int bas, int uz)
{

char*x;
x=new char[uz+1];
int i;
    for(i=0;i<uz;i++)
        x[i]=k[i+bas];

    x[i]=0;

return x;

}


int evaluatorBase::sayimi(char*x)
{

int boy=strlen(x),i,virgul=0;
char c;

for(i=0;i<boy;i++){
c=x[i];
    if(!(c<='9' && c>='0'))
        if(c=='.')
            if(virgul)
                return 0;
            else
                virgul++;
        else
            return 0;
}

return 1;

}

int evaluatorBase::sayisalmi(char*x)
{

int boy=strlen(x),i,virgul=0;
char c;


for(i=0;i<boy;i++){
c=x[i];
    if(!(c<='9' && c>='0'))
        if(c=='.'){
            if(virgul)
                return 0;
            else
                virgul++;
        }
        else if(c=='-'){
            if(i>0)
                return 0;
        }
        else
            return 0;
}

return 1;

}



evVar evaluatorBase::variable(int varNo){
 return vars[varNo];
}


void evaluatorBase::degisken_sil(int deg_no){
    vars[deg_no].setEnabled(0);
}

void evaluatorBase::degisken_sil(const char*isim){
    int i=deg_no_bul(isim);
    vars[i].setEnabled(0);
}

void evaluatorBase::degisken_ekle(evVar*dgs,int address){

    vars[address].setName(dgs->getName());
    vars[address].setValue(dgs->getValue());
    vars[address].setEnabled(dgs->getEnabled());
    vars[address].setFixed(dgs->getFixed());


}


void evaluatorBase::saveVarsToFile()
{
    FILE*file;
    file=fopen(variable_file,"w");
    int i=0;

    for(i=0;i<100;i++)
    if(vars[i].getEnabled())
        fprintf(file,"%s \t %d \t %f\n",vars[i].getName(),vars[i].getFixed(),vars[i].getValue());

    fprintf(file,"#end");
    fclose(file);


}

int evaluatorBase::num_of_functions(){
    return fonksiyonsay;
}

evFunc*evaluatorBase::get_function(int no){
    return fx[no];
}

void evaluatorBase::load_function(evFunc*fnc,int fNo){


    fx[fNo]=fnc;

}



#define makeLineDebug 1

int evaluatorBase::makeLine(char*expression, evFunc*fnc)
{


elemansay=0;

int i,j;
char c,isl[100];
strcpy(isl,expression);

int elboy=0;

int elkon=0,eluz=0;
int boy=strlen(isl);

if(boy==0)
    return -1;

fnc->setAnswerAdress(0);

for(i=0;i<boy;i++)
{

    c=isl[i];

    if(c=='(' || c==')' || c==',' || c=='-' ){

        if(eluz>0)
        koda_ekle(aradan_al(isl,elkon,eluz),0,0,0,-1,0);
        eluz=0;elkon=-1;
        koda_ekle(&c,c,0,0,-1,0);
    }


    else if(c=='+' || c=='*' || c=='/'  || c=='^' || c=='<' || c=='>' || c=='%' || c=='&' || c=='|' || c==':'){
        if(eluz>0)
        koda_ekle(aradan_al(isl,elkon,eluz),0,0,0,-1,0);
        eluz=0;elkon=-1;
        koda_ekle(&c,'o',0,0,-1,0);
    }

    else if(c==' ')
    {
        if(eluz>0)
            koda_ekle(aradan_al(isl,elkon,eluz),0,0,0,-1,0);

        eluz=0;elkon=-1;

    }

    else if(c=='='){
        if(eluz>0){
            if(deg_no_bul(aradan_al(isl,elkon,eluz))>=0){
            if(vars[deg_no_bul(aradan_al(isl,elkon,eluz))].getFixed()){
                strcpy(errorStr,"sabitleri degistiremezsiniz:");
                strcat(errorStr,aradan_al(isl,elkon,eluz));
                return -1;
            }
            fnc->setAnswerAdress(deg_no_bul(aradan_al(isl,elkon,eluz)));
            }
            else{
            if(degisken_ekle(aradan_al(isl,elkon,eluz),0,0,-1)>0)
               fnc->setAnswerAdress(deg_no_bul(aradan_al(isl,elkon,eluz)));
            else{

                strcpy(errorStr,"degisken eklenemedi: ");
                strcat(errorStr,aradan_al(isl,elkon,eluz));

                return -1;

            }
            }
        }
        else
        return -1;
        eluz=0;elkon=-1;
        }

    else
    {
        if (elkon<0)
            elkon=i;

        if(eluz)
            if(sayimi(aradan_al(isl,elkon,eluz)))
                if(!sayimi(aradan_al(isl,elkon,eluz+1))){
                    koda_ekle(aradan_al(isl,elkon,eluz),0,0,0,-1,0);
                    elkon=i;eluz=0;
                    }

        eluz++;



    }
}

if (eluz)
    koda_ekle(aradan_al(isl,elkon,eluz),0,0,0,-1,0);




if(!elemansay){

    strcpy(errorStr,"islemde oge bulunamadi");

    return -1;
}




//kural 1: parantezler e�le�meli.
///////////////////////////////////////////////

int parder=0;

for(i=0;i<elemansay;i++)
{

    if(elemanlar[i].g1=='(')
    parder++;
    else if(elemanlar[i].g1==')')
    parder--;

    if(parder<0){
    strcpy(errorStr,"parantez uyusmazligi hatasi");
    return -1;
    }

    elemanlar[i].g2=parder;



}



if(parder!=0){
    strcpy(errorStr,"parantez uyusmazligi hatasi");
    return -1;
}


///////////////////////////////////////////////



//fonksiyon, say� ya da de�i�kenler ayr�lacak..

for(i=0;i<elemansay;i++)
{
    if(elemanlar[i].g1==0){//belirsiz eleman
    if(sayimi(elemanlar[i].isim))
        elemanlar[i].g1='s'; //say�
    else if(i<elemansay-1 && elemanlar[i+1].g1=='(') //fonksiyonlar her zaman ( ile devam eder
        elemanlar[i].g1='f';
    else
        elemanlar[i].g1='d';
    }

}

//parantezler kald�r�lacak.
for(i=0;i<elemansay;i++)
    if(elemanlar[i].g1=='(')
    if(elemanlar[i-1].g1=='f'){
        koddan_sil(i);i--;}
    else
        koda_ekle("HESAPLA",'f',elemanlar[i].g2-1,0,i,1);

    else if(elemanlar[i].g1==')'){
        koddan_sil(i);i--;}


// '-' i�areti operat�r m�, sonrakini negatif mi yap�yor?

for(i=0;i<elemansay;i++)
{
    j=elemanlar[i].g2;
    if(elemanlar[i].g1=='-')
    {
    if(i==0 ){//ba�ta ise, negatif g�revi g�r�yor, bu durumda sonrakinin negatif bayra��n� de�i�tir.
        if(i==elemansay-1){
        strcpy(errorStr,"islec basta ya da sonda olamaz");
        return -2;
        }
        elemanlar[i+1].g3=(1-elemanlar[i].g3);
        koddan_sil(i);i--;

    }
    else if(elemanlar[i-1].g1=='o' || elemanlar[i-1].g2<elemanlar[i].g2 || elemanlar[i-1].g1==','){
        if(i==elemansay-1)
        {	strcpy(errorStr," sonda virgul veya islec olamaz" );
        return -2;
        }

       elemanlar[i+1].g3=(1-elemanlar[i].g3);
       koddan_sil(i);i--;
    }
    else
        elemanlar[i].g1='o';


    }


}

/////////////////////////////////////////////////////////
//say�dan sonra gelen fonksiyon, say� ya da parantezse araya * koy.

for(i=0;i<elemansay-1;i++)
    if(elemanlar[i].g1=='s')
    if(elemanlar[i+1].g1=='(' || elemanlar[i+1].g1=='d' || elemanlar[i+1].g1=='f'){
        koda_ekle("*",'o',elemanlar[i].g2,0,i+1,0);i++;}



//fonksiyonlar�n varl��� ara�t�r�lacak.;

for(i=0;i<elemansay;i++)
    if(elemanlar[i].g1=='f'){

    j=fnobul(elemanlar[i].isim);
    elemanlar[i].isim[19]=j;
    if(j<0){
        strcpy(errorStr,"islev bulunamadi:");
        strcat(errorStr,elemanlar[i].isim);
        return -4;
    }

    }



//say�lar bellege at�lacak, bellek 0. fonksiyonda saklanacak
//fnc->setMemoryInputNum(0);

for(i=0;i<elemansay;i++)
    if(elemanlar[i].g1=='s'){
    j=1;
    if(elemanlar[i].g3==1)
        j=-1;
    fnc->setMemory(fnc->getMemoryInputNum(),j*atof(elemanlar[i].isim));
    elemanlar[i].isim[19]=fnc->getMemoryInputNum();
    fnc->incMIN();
    }







//fonksiyonlar�n giri� say�lar�n�n do�rulu�una bakaca��z. giri� say�s�= i�erdeki virg�l say�s�+1
int fder=0;
int girsay=0;
for(i=0;i<elemansay;i++)
    if(elemanlar[i].g1=='f'){
    girsay=0;
    fder=elemanlar[i].g2;//fonksiyonun par. derecesi
    j=i+1;
    while(elemanlar[j].g2>fder){
        if(elemanlar[j].g2==fder+1 && elemanlar[j].g1==',')
        girsay++;
        j++;
    }
    if(j>i+1)
        girsay++;

    if(!(fx[elemanlar[i].isim[19]]->getInputNum() ==girsay))//giri� say�s� kontrol�
    {  strcpy(errorStr,"islev giris sayisi uyumsuz:");
       strcat(errorStr,fx[elemanlar[i].isim[19]]->getName());
       return -5;
    }

    }



//kural: ba�ta ya da sonda operat�r olamaaz;
if(elemanlar[0].g1=='o'|| elemanlar[elemansay-1].g1=='o'){
    strcpy(errorStr,"islec basta ya da sonda olamaz");
    return -2;
   }

//ba�ta sonda virg�l olamaz.
if(elemanlar[0].g1==',' || elemanlar[elemansay-1].g1==','){
    strcpy(errorStr,"virgul basta ya da sonda olamaz");
    return -2;
}


for(i=0;i<elemansay;i++){
        //ardarda operat�r olmaaz
    if(elemanlar[i].g1=='o')
    if(elemanlar[i-1].g1=='o'){
        strcpy(errorStr,"ardarda iki islec gelemez");
        return -3;
    }
    //iki say� ardarda gelemez
    if(elemanlar[i].g1=='s')
    if(i <elemansay-1 && elemanlar[i+1].g1=='s'){
        strcpy(errorStr,"ardarda iki sayi gelemez");
        return -6;
    }

    //operat�r�n �n�nde parantez olamaz (7*)8 olamaz
    if(elemanlar[i].g1=='o')
    if(elemanlar[i].g2>elemanlar[i+1].g2){
        strcpy(errorStr,"islecin hemen onunde parantez olamaz");
        return -7;
    }

    //operat�r�n arkas�nda parantez olamaz. 3(*8) olamaz
    if(elemanlar[i].g1=='o')
    if(elemanlar[i].g2>elemanlar[i-1].g2){
        strcpy(errorStr,"islecin hemen arkasinda parantez olamaz");
        return -8;
    }


    //operat�r�n arkas�nda, �n�nde  "," olamaz.
    if(elemanlar[i].g1=='o')
    if(elemanlar[i-1].g1==',' || elemanlar[i+1].g1==','){
     strcpy(errorStr,"islecten hemen sonra ya da once virgul olamaz ");
        return -9;
    }


    //virg�l�n �n�nde parantez olamaz.
    if(elemanlar[i].g1==',')
    if(elemanlar[i].g2>elemanlar[i+1].g2){
        strcpy(errorStr,"virgulun onunde parantez olamaz");
        return -10;
    }

    //virg�l�n parantez derecesi 0 olamaz
    if(elemanlar[i].g1==',' && elemanlar[i].g2==0){
    strcpy(errorStr,"virgul,bir fonksiyona ait degil");
    return -11;
    }

    //degisken var m�, varsa yerini al.
    if(elemanlar[i].g1=='d'){
    j=deg_no_bul(elemanlar[i].isim);
    if(j<0){
        strcpy(errorStr,"degisken bulunamadi:\n");
        strcat(errorStr,elemanlar[i].isim);
        return -12;
    }
    elemanlar[i].isim[19]=j;
    }


    //virg�l silinsin

    if(elemanlar[i].g1==','){
    koddan_sil(i);i--;
   }

}

//g�r�n�rde bir hata yok..., �imdi, i�lem s�ras�na g�re s�ralanacak.
int oncelik=0;
int k=0,l=0,p=0,q=0,m=0;

int sira[256]={0};

//2n= oncelik no;
//2n+1= konum(i�aret etti�i)
//255=adet



//islem sirasi bulunuyor..
for(i=0;i<elemansay;i++){

 oncelik=0;
 c=elemanlar[i].g1;
 oncelik=elemanlar[i].g2*80;

 if(c=='d' || c=='o' || c=='f')
 {
     if(c=='d')
     oncelik+=40;

     else if(c=='f')
     oncelik+=20;

     else if(c=='o'){
     c=elemanlar[i].isim[0];


     if(c=='+')
         oncelik+=6;
     else if(c=='-')
         oncelik+=7;
     else if(c=='*')
         oncelik+=8;
     else if(c=='/')
         oncelik+=9;
     else if (c=='^')
         oncelik+=10;
     else if(c=='<')
         oncelik+=4;
     else if(c=='>')
         oncelik+=5;

     else if(c==':')
         oncelik+=1;
     else if(c=='&')
         oncelik+=2;
     else if(c=='|')
         oncelik+=3;

     }



     for(j=0;j<sira[255];j+=2)
     if (sira[j]<oncelik)
         break;

     for(k=sira[255];k>j;k-=2){
     sira[k+1]=sira[k-1];
     sira[k]=sira[k-2];
     }

     sira[255]+=2;
     sira[j]=oncelik;
     sira[j+1]=i;

 }

}
#if makeLineDebug==1 && KDEBUG==1

for(i=0;i<sira[255];i+=2)
    fprintf(fdebug,"MLD> %d \t %d\n", sira[i], sira[i+1]);


fprintf(fdebug,"islem elemanlari:\n");
fprintf(fdebug,"toplam eleman say= %d \n", elemansay);

fprintf(fdebug,"eleman  ;bay1, bay2, bay3 20.kar\n");
for(i=0;i<elemansay;i++){
    c=elemanlar[i].g1;
    if(c=='-' || c=='(' || c==')' || c==',' || c=='o')
    fprintf(fdebug,"%c",elemanlar[i].isim[0]);
    else
    fprintf(fdebug," MLD> %s\n", elemanlar[i].isim);

    fprintf(fdebug," MLD> \t %c \t %d \t %d \t%d\n",elemanlar[i].g1,elemanlar[i].g2,elemanlar[i].g3,elemanlar[i].isim[19]);

}

#endif



for(i=0;i<sira[255];i+=2)
{
j=sira[i+1];
c=elemanlar[j].g1;

if(c=='d'){
    fnc->addCode(1,elemanlar[j].isim[19],fnc->getMemoryInputNum());

    if(elemanlar[j].g3==1)//-1 bayra��
        fnc->addCode(36,fnc->getMemoryInputNum(),fnc->getMemoryInputNum());

    elemanlar[j].isim[19]=fnc->getMemoryInputNum();
    fnc->incMIN();
}

else if(c=='f'){
    for(m=0;m<fx[elemanlar[j].isim[19]]->getInputNum() ;m++){

        fnc->addCode(5,elemanlar[j+1].isim[19]);
    koddan_sil(j+1);
    fnc->addCode(m+1);

    for(l=0;l<sira[255];l+=2)
        if(sira[l+1]>j)
        sira[l+1]--;
    }

    fnc->addCode(23,elemanlar[j].isim[19],4,0);
    fnc->addCode(fnc->getMemoryInputNum());

    if(elemanlar[j].g3==1)//-1 bayra��
    fnc->addCode(36,fnc->getMemoryInputNum(),fnc->getMemoryInputNum());


    elemanlar[j].isim[19]=fnc->getMemoryInputNum();
    fnc->incMIN();
}

else if(c=='o'){
    c=elemanlar[j].isim[0];

    if(c=='+')
    m=6;
    else if(c=='-')
    m=7;
    else if(c=='*')
    m=8;
    else if(c=='/')
    m=9;
    else if(c=='<')
    m=25;
    else if(c=='>')
    m=26;
    else if(c==':')
    m=27;
    else if(c=='&')
    m=29;
    else if(c=='|')
    m=28;
    else if(c=='^')
    m=24;

    fnc->addCode(m,elemanlar[j-1].isim[19],elemanlar[j+1].isim[19],fnc->getMemoryInputNum());

    koddan_sil(j+1);
    koddan_sil(j-1);
    elemanlar[j-1].isim[19]=fnc->getMemoryInputNum();
    fnc->incMIN();


    for(l=0;l<sira[255];l+=2)
    if(sira[l+1]>j)
        sira[l+1]-=2;

}


}

if(fnc->getAnswerAddress())
    fnc->addCode(46,fnc->getMemoryInputNum()-1,fnc->getAnswerAddress());



#if makeLineDebug==1 && KDEBUG==1
fprintf(fdebug,"MLD>\nfonksiyon[0] bilgisi:\n");
fprintf(fdebug,"MLD>bellek:\n");
fprintf(fdebug,"MLD>bellek gir. say=%d\n",fnc->MemoryInputNum);
for(i=0;i<fnc->MemoryInputNum;i++)
    fprintf(fdebug,"MLD>%f\n",fnc->Memory[i]);

fprintf(fdebug,"MLD>kod:\n");
for(i=0;i<fnc->CodeSize;i++)
    fprintf(fdebug,"MLD>%d \n" ,fnc->Code[i]);

#endif


return 1;

}





#define debugDerleyiciK1 1
#define debugDerleyiciK2 1
#define debugDerleyiciK3 1

int evaluatorBase::evaluatorBase_Derle(char*derlenecek){



    //1.k�s�m, parcalara ayirma
    int i=0,j,k;
    int kps=0,der=0;
    char h;
    char*hh;

    int derece=0;
    sats=0;
    els=0;
    char x[256],y[256];


        #if debugDerleyiciK2==1 && KDEBUG==1
            fprintf(fdebug,"debugDerleyiciK1:parcalanma asamasi\n");
        #endif

    j=0;
    bool bul=false;



    for(i=0;i<kodboyu;i++){
        if(derlenecek[i]==';')
            bul=true;

        #if debugDerleyiciK2==1 && KDEBUG==1
            fprintf(fdebug,"debugDerleyiciK1:%d.karakter=%c (%d) \n",i,derlenecek[i],derlenecek[i]);
        #endif



        if(derlenecek[i]==13)
            if(i<kodboyu-1)
                if(derlenecek[i+1]==10)
                    bul=true;


        if(derlenecek[i]==32)
            if(i<kodboyu-1)
                if(derlenecek[i+1]==10){
                    bul=true;
                    i=i+2;
                }

        if(bul ){

                strcpy(y,aradan_al(derlenecek,j,i-j));
                if(bosluk(y,0,-1)){
                    bul=false;
                    continue;
                }

                satirlar[sats]=new char[i-j+1];
                strcpy(satirlar[sats],aradan_al(derlenecek,j,i-j));

                j=i+1;


        boslukSil(satirlar[sats]);


#if debugDerleyiciK1==1 && KDEBUG==1
fprintf(fdebug,"debugDerleyiciK1:alinan:%d satir: -%s-\n",sats,satirlar[sats]);
#endif


        //bosluklari da yokedecez..

        bul=false;
        sats++;

        }

    }




    if(sats==0)
        return -1;


    //2.k�s�m: parcalari d�zenleme.. ve genel yapiyi inceleme, gidis
    //adreslerini belirleme..



    int err,tt;

    for(i=0;i<sats;i++){

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> incelenen: %s\n",satirlar[i]);
#endif



        strcpy(x,satirlar[i]);
        j=sonrakiParca(x,0,&tt);



        strcpy(y,aradan_al(x,0,j));
        boslukSil(y);

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> alinan bolum -%s- ile:%d \n",y,tt);
#endif



if(strcmp(y,"if")==0){////////////////////////////////////////////////

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> bolum %s bir if cumlesi\n",y);
#endif

            //format= if(EXP) [;]
            //			EXP;
            //			EXP;
            //			EXP;
            //			end


            //kosul c�mlesi cikarma..
            //kosul c�mlesi ac parantez ile baslar..
#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> ( var mi bakiliyor.. \n");
#endif
            if(tt!=5)
                return -1;

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> ( var mi bakildi\n");
#endif

            //kosul c�mlesi baslangici olan parantezden kapanis parantezine kadar olan ara bulunacak..

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> kosul araniyor.. \n");
#endif
            err=sonrakiParca(x,j,&tt);
            if(err<0)
                return -2; // parantez hatasi..

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> kosul bulundu.. %d de kapanan par.\n",err);
#endif

            //2 kosul var:
            //1. kosul c�mlesi bos olamaz.
#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> kosul bos mu?\n");
#endif

        if (bosluk(x,j,err))
                return -3;

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> kosul bos degil:)\n");
#endif



            //2. kosul c�mlesinin kapanmasindan sonra bi sey gelemez..
#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> kosulun devami bosmu? \n");
#endif

            if(!bosluk(x,err+1,-1))
                return -4;

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> kosulun devami bos :) \n");
#endif


#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> kosul cumlemiz kopyalaniyor.. \n");
#endif
            emirler[els]=new char[err-j-1];
            strcpy(emirler[els],aradan_al(x,j+1,err-j-1));//kosul c�mlemiz..

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> kosul cumlemiz kopyalandi (adr=%d) :) -%s- \n",els,emirler[els]);
#endif
            turler[els]=1;//if kosul c�mlesi..
            adresler[els][0]=els+1;
            adresler[els][1]=-1;//hen�z belli de�il
            els++;


        }
        else if(strcmp(y,"else")==0){////////////////////////////////////////////////

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> bolum %s bir else cumlesi\n",y);
#endif

            //format:  else [;]
            //else ile kosul yanlis ise buraya atlanilir..
            //kural 1: else ten sonrasi bos olacak..

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> devami bosluk mu?\n");
#endif
            if(!bosluk(x,j,-1))
                return -5;

#if(debugDerleyiciK3==1&& KDEBUG==1)
    fprintf(fdebug,"ddk3> devami bosluk :)\n");
#endif

            // kural2: else kendindenden �nce bir adet adreslenmemis if gerektirir.

#if(debugDerleyiciK3==1&& KDEBUG==1)
    fprintf(fdebug,"ddk3> %d den onceki if araniyor.. \n",els);
#endif

            for(k=els-1;k>=0;k--)
                if(turler[k]==1 && adresler[k][1]==-1){
                    adresler[k][1]=els;//+1 de�il..
                    break;
                }

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> arama sonuc adresi: %d\n",k);
#endif

            if(k<0)
                return -6;



#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> duzgunluk analizi 7 \n");
#endif
            //kural: else ile karsi d�sen if arasinda yalnizca d�z islem olacak..
            k++;
            for(;k<els;k++){
                if(adresler[k][1]==-1 || adresler[k][0]==-1){

                    #if(debugDerleyiciK3==1 && KDEBUG==1)
                    fprintf(fdebug,"ddk3> %s den dolayi hatali.. \n",emirler[k]);
                    #endif


                    return -7;

                }
            }

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> duzgunluk analizi 7 :) \n");
            #endif

            //kendimizden �ncekinin birincil adresini -1 yapip t�r�n� de�i�tirecegiz.
            //��nk� bizden �nceki adres artik "end" ile de�i�ecek..

            adresler[els-1][0]=-1;
            turler[els-1]=2;//endin sonuna atlayacak islem..

        }




        else if(strcmp(y,"for")==0){
            return -99;


        }
        else if(strcmp(y,"end")==0){////////////////////////////////////////
                //return -99;
                //format:  end [;]
            //end kendinden �nceki for, while vb.. kapatir..


            //kural  end ten sonrasi bos olacak..
            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> bolum %s bir end cumlesi\n",y);
            #endif

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> sonrasinda bosluk var mi? \n",y);
            #endif


            if(!bosluk(x,j,-1))
                return -8;

                #if(debugDerleyiciK3==1 && KDEBUG==1)
                    fprintf(fdebug,"ddk3> sonrasinda bosluk var:) \n",y);
                #endif

            //end den �nceki tamamlanmamis adres bulunacak.

                #if(debugDerleyiciK3==1 && KDEBUG==1)
                    fprintf(fdebug,"ddk3> tamamlanmamis adres araniyor.) \n");
                #endif


                for(k=els-1;k>=0;k--)
                    if(adresler[k][1]==-1 || adresler[k][0]==-1)
                        break;

                #if(debugDerleyiciK3==1 && KDEBUG==1)
                    fprintf(fdebug,"ddk3> tamamlanmamis adres : %d , tur=%d \n",k,turler[k]);
                #endif

                if(k<0)
                    return -10;


                #if(debugDerleyiciK3==1 && KDEBUG==1)
                    fprintf(fdebug,"ddk3> %d nin yeni sonraki adresi: %d \n",k, els);
                #endif

                    if(turler[k]==1){//if ko�ulu..(else sahibi de�ilse)
                        adresler[k][1]=els;

                    }

                    if(turler[k]==2){//else den bir �nceki eleman
                        adresler[k][0]=els;
                        turler[k]=0;
                    }

                    if(turler[k]==3){//while kosulu..

                        adresler[k][1]=els+1;

                        adresler[els][0]=k;

                        turler[els]=32;//kosulsuz atlama �zel komut(derlenmeyecek..)
                        emirler[els]=new char[10];
                        strcpy(emirler[els],"ozelKomut");

                        els++;

                        //adresler[els-1][0]=k;//end den onceki eleman d�ng�de ��nk�.


                    }

        }
        else if(strcmp(y,"while")==0){


            //format= if(EXP) [;]
            //			EXP;
            //			EXP;
            //			EXP;
            //			end


            //kosul c�mlesi cikarma..
            //kosul c�mlesi ac parantez ile baslar..
            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> while bulundu, ( var mi bakiliyor.. \n");
            #endif

            if(tt!=5)
                return -1;

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> ( var :)\n");
            #endif

            //kosul c�mlesi baslangici olan parantezden kapanis parantezine kadar olan ara bulunacak..

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> kosul araniyor.. \n");
            #endif

            err=sonrakiParca(x,j,&tt);
            if(err<0)
                return -2; // parantez hatasi..

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> kosul bulundu :) %d de kapanan par.\n",err);
            #endif

            //2 kosul var:
            //1. kosul c�mlesi bos olamaz.

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> kosul bos mu?\n");
            #endif

            if (bosluk(x,j,err))
                return -3;

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> kosul bos degil:)\n");
            #endif



            //2. kosul c�mlesinin kapanmasindan sonra bi sey gelemez..
            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> kosulun devami bosmu? \n");
            #endif

            if(!bosluk(x,err+1,-1))
                return -4;

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> kosulun devami bos :) \n");
            #endif


            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> kosul cumlemiz kopyalaniyor.. \n");
            #endif

            emirler[els]=new char[err-j-1];
            strcpy(emirler[els],aradan_al(x,j+1,err-j-1));//kosul c�mlemiz..

            #if(debugDerleyiciK3==1 && KDEBUG==1)
                fprintf(fdebug,"ddk3> kosul cumlemiz kopyalandi (adr=%d) :) -%s- \n",els,emirler[els]);
            #endif

            turler[els]=3;//while kosul c�mlesi..
            adresler[els][0]=els+1;
            adresler[els][1]=-1;//hen�z belli de�il
            els++;


        }
        else{////////////////////////////////////////////////////////////

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> %s  bolumu bir islem, adres=%d olacak\n",x,els);
#endif

        emirler[els]=new char[strlen(x)];
        strcpy(emirler[els],x);


        adresler[els][0]=els+1;
        adresler[els][1]=-2;	//2. adres olamaz..
        turler[els]=0;//d�z kod..
        els++;

        }



}



    //kural: acikta adres kalmamali.
    for(i=0;i<els;i++){

        if(adresler[i][1]==-1 || adresler[i][0]==-1){
                #if(debugDerleyiciK3==1 && KDEBUG==1)
                    fprintf(fdebug,"%s icin karsilik gelen end bulunamadi!\n",emirler[i]);
                #endif

                return -1;

        }

    }


    #if(debugDerleyiciK3==1 && KDEBUG==1)
                    fprintf(fdebug,"ddk3> adresleme ve siniflama sonuc tablosu:\n");
                    fprintf(fdebug,"adres\t  eleman \t  tur\t sonraki#0 \t sonraki#1 \n");
                        for(j=0;j<els;j++){
                            fprintf(fdebug,"%d \t %s \t %d \t %d \t %d  \n",j,emirler[j],turler[j],adresler[j][0],adresler[j][1]);


                        }

    #endif




    /////////////////////////////////////////////////////////



    return 1;
}

int KediDerleyici_Derle(const char*kod,evFunc*fnc){








return 1;

}




int evaluatorBase::harfbul(char*k,char harf,int basl){

    int i,boy=strlen(k);
    for(i=basl;k[i];k++)
        if(k[i]==harf)
            return i;

    return -1;

}

int evaluatorBase::sonrakiKarakter(char*k,int basl){

    int i=basl+1;

    while(k[i]){
        if(k[i]==' ')
            continue;
        else
            return i;

        i++;

    }

    return 0;

}

#define debugBoslukSil 0

void evaluatorBase::boslukSil(char*k){

    //1.adim, bastan ilk karaktere kadar..
    int i=0,b,s,l=strlen(k);

    while(k[i]==' ' || k[i]==32 || k[i]==10 || k[i]==13)
        i++;

#if debugBoslukSil==1 && KDEBUG==1
    fprintf(fdebug,"debugBoslukSil: bastan %d. karaktere kadar\n",i);
#endif

    b=i;



    //2.adim, sondan basa gitme..
    i=l;

    while(k[i]==' ' || k[i]==32 || k[i]==10 || k[i]==13 || k[i]==0)
        i--;


#if debugBoslukSil==1 && KDEBUG==1
    fprintf(fdebug,"debugBoslukSil: sondan %d. karaktere kadar\n",i);
#endif

    s=i;


    //3.adim, bu ikisi arasindaki karakterleri kopyalama..


    for(i=b;i<=s;i++)
        k[i-b]=k[i];


    k[i-b]=0;//sonlandirma..

#if debugBoslukSil==1 && KDEBUG==1
    fprintf(fdebug,"debugBoslukSil: sonuc -%s- \n",k);
#endif

}




#define debugBosluk 0
bool evaluatorBase::bosluk(char*k, int bas=0, int son=-1){

    if(son<0)
        son=strlen(k);

#if debugBosluk==1 && KDEBUG==1
    fprintf(fdebug,"db> %s bos mu bakilacak (%d %d arasi)\n",k,bas,son);
#endif


    if(son>strlen(k))
        son=strlen(k);

    if(bas>=son){

#if debugBosluk==1 && KDEBUG==1
    fprintf(fdebug,"db> basi >= son  oldugundan bos diyorum.\n");
#endif

        return true;

    }
    int i;

    for(i=bas;i<son;i++)
        if(k[i]!=' ' && k[i]!=32 && k[i]!=10 && k[i]!=13){
            #if debugBosluk==1 && KDEBUG==1
                fprintf(fdebug,"db>  bos degil..\n");
            #endif

            return false;


        }

#if debugBosluk==1 && KDEBUG==1
    fprintf(fdebug,"db>  bos..\n");
#endif


    return true;

}

#define debugSonrakiParca 0

 int evaluatorBase::sonrakiParca(char*k,int konum,int*tur){

    //bu fonksiyon sayesinde k daki ilk anlamli parcayi cekecez.
    int i=konum;
    int j=konum;

    int pder=0;
    int gpder=0;

#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> %s incelenecek..\n", &k[konum]);
#endif

    if(k[i]=='('){

#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> parantez modu\n", &k[konum]);
#endif

        //bu parantez kapanana kadar olan yer..
        pder=1;
        i++;

        while(1){

            if(k[i]=='(')
                pder++;

            if(k[i]==')'){


                pder--;

#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> kapanan bir tane bulundu, %d \n",pder);
#endif

            }

            if(pder==0){

#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> kapatan bulundu\n");
#endif
            return i;


            }
            if(pder<0){

#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> parantez uyusmuyor!\n");
#endif
            return -1;

            }

        i++;

        if(i>strlen(k)){

#if(debugDerleyiciK3==1 && KDEBUG==1)
    fprintf(fdebug,"ddk3> parantez modu kapanan parantez bulamadi!\n");
#endif

        *tur=-1;
        return -1;


        }

    }


}



    else if(k[i]==' '){

#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> bosluk modu\n", &k[konum]);
#endif

        while(k[i]==' '){
            i++;

            if(!k[i])
                return -1;
        }


        *tur=3;//bosluk b�lgesi..
        return i;
    }


    else {
#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> normal mod\n");
#endif

        while(k[i]){


            if(k[i]==';'){
                *tur=4;
                break;
            }
            else if(k[i]=='('){

#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> acilan parantez bulundu..\n");
#endif
                *tur=5;
                break;

            }

            else if(k[i]==')'){
                *tur=6;
                break;
            }
            else if(k[i]=='{'){
                *tur=7;
                break;
            }

            else if(k[i]==' '){
                //bosluk olursa, b
                while(k[i+1]==' '){
                        i++;

                if(!k[i])
                        break;
                }



            }

            i++;

        }

        if(!k[i])
            *tur=-1;

#if(debugSonrakiParca==1 && KDEBUG==1)
    fprintf(fdebug,"dsp> normal mod bitti ile:%d\n",*tur);
#endif

        return i;

    }

}
