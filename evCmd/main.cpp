#include <iostream>
#include "../common/evaluatorbase.h"

#ifndef FUNCTIONS
#define FUNCTIONS   "../math/functions.txt"
#endif

#ifndef VARIABLES
#define VARIABLES   "../math/variables.txt"
#endif


using namespace std;

//char example[]={"X=1;Y=5 \n if(X<2) \n Y=Y-1  ;X=X+2;  if(Y>0); X=X*2; end;  else \n Y=Y+1; end; "};
char example[]="(3+4)*(7+2) \n";

int main()
{
    char calcstr[100];
    printf("evaluater cmd!\n");
    evaluatorBase*evb;
    evFunc*evf;
    evb=new evaluatorBase(FUNCTIONS,VARIABLES);
    evf=new evFunc();
    int x;


while(1)
{
    printf(">");
    gets(calcstr);



    if(calcstr[0]=='#')
    {
        if(!strcmp(1+calcstr,"vars"))
        {
            printf("current variables\n");
            for(int i=0;i<100;i++){
                if(evb->vars[i].Enabled)
                    printf("%s=%f \n",evb->vars[i].Name,evb->vars[i].Value);
            }


            continue;
        }
        else if(!strcmp(1+calcstr,"functions"))
        {
            printf("current functions\n");
            for(int i=0;i<100;i++){
                if(evb->fx[i]->CodeSize){
                    printf("%s\n",evb->fx[i]->Name);
                    printf("%s\n",evb->fx[i]->ACIKLAMA);
                    printf("\n");
                }
            }
            continue;
        }
        else if(!strcmp(1+calcstr,"quit"))
        {
            printf("bye!\n");
            delete evf;
            delete evb;
            exit(0);
        }
        else {
            printf("command not found\n");
            continue;
        }
    }


    x=strlen(calcstr);
    calcstr[x]=32;
    calcstr[x+1]=10;
    calcstr[x+2]=0;

    if(evb->make2(calcstr,evf)<0){
        printf("ERR: %s \n",evb->getErrorStr());
        continue;
    }

    if(evb->isle(evf)<0){
        printf("a logical error in calculation\n");
        continue;
    }

    int i;

    for(i=0;i<100;i++){
        if(evb->vars[i].Enabled && !strcmp("ans",evb->vars[i].Name))
            printf("%s=%f \n",evb->vars[i].Name,evb->vars[i].Value);
    }

}
    printf("exiting\n");
    return 0;

}
