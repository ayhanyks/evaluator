TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ../common/evvar.cpp \
    ../common/evfunc.cpp \
    ../common/evaluatorbase.cpp

HEADERS += \
    ../common/evvar.h \
    ../common/evfunc.h \
    ../common/evaluatorbase.h
